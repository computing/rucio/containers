#!/bin/bash

echo -e "#!/bin/bash\necho $SSH_PASSWORD" > print-pass.sh
chmod +x print-pass.sh

echo "Starting ssh-agent"
if [ -z "$SSH_AUTH_SOCK" ] ; then
  eval $(ssh-agent -s)
  DISPLAY=1 SSH_ASKPASS="./print-pass.sh" ssh-add < /dev/null
fi
echo "Listing identities:"
ssh-add -l

rm print-pass.sh

eval $@

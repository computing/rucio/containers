# IGWN Rucio Clients

Client container for IGWN Rucio deployment.

This is built from the upstream [`rucio/rucio-clients`](https://hub.docker.com/r/rucio/rucio-clients) image with additions for:

* Registration utilities
* Virgo VOMS information


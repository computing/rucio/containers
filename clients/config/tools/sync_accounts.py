#!/usr/bin/env python3
# Copyright 2013-2020 CERN for the benefit of the ATLAS collaboration.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Authors:
# - James Alexander Clark <james.clark@ligo.org>, 2021

import sys
import os.path
base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(base_path)
os.chdir(base_path)

import json  # noqa: E402
import sys  # noqa: E402
import traceback  # noqa: E402

from rucio.client import Client  # noqa: E402
from rucio.common.exception import Duplicate  # noqa: E402

UNKNOWN = 3
CRITICAL = 2
WARNING = 1
OK = 0


def main(argv):
    # parameters
    if argv:
        account_repo_file = argv[0]
    else:
        account_repo_file = 'etc/account_repository.json'

    json_data = open(account_repo_file)
    repo_data = json.load(json_data)
    json_data.close()

    c = Client()
    for account in repo_data:

        # Create account
        try:
            account_type = repo_data[account].get('type')
            email = repo_data[account].get('email')
            c.add_account(account, type_=account_type, email=email)
        except Duplicate:
            print('%(account)s already added' % locals())
        except:
            errno, errstr = sys.exc_info()[:2]
            trcbck = traceback.format_exc()
            print('Interrupted processing with %s %s %s.' % (errno, errstr, trcbck))

        # Set attributes
        for key in repo_data[account]['attrs']:
            value = repo_data[account]['attrs'][key]
            try:
                c.add_account_attribute(account, key, value)
            except Duplicate:
                print('%(account)s already has attribute %(key)s' % locals())
            except:
                errno, errstr = sys.exc_info()[:2]
                trcbck = traceback.format_exc()
                print('Interrupted processing with %s %s %s.' % (errno, errstr, trcbck))

        # Set global storage limits
        try:
            global_limit = repo_data[account].get('global_limit', -1)
            for rse in [r['rse'] for r in c.list_rses()]:
                c.set_account_limit(account=account, rse=rse, bytes_=global_limit, locality='local')
        except:
            errno, errstr = sys.exc_info()[:2]
            trcbck = traceback.format_exc()
            print('Interrupted processing with %s %s %s.' % (errno, errstr, trcbck))

        # Set identities
        for ident in repo_data[account]['identities']:
            try:
                i = repo_data[account]['identities'][ident] 
                identity = i['id']
                authtype = i['type']
                default = i['default']
                email = i['email']
                try:
                    password = i['password']
                except KeyError:
                    password = None
                c.add_identity(account, identity, authtype, email, default=default, password=password)
            except ValueError as e:
                print(account, e)
            except Duplicate as e:
                print('%(account)s already has identity %(ident)s' % locals())
            except Exception:
                errno, errstr = sys.exc_info()[:2]
                trcbck = traceback.format_exc()
                print('Interrupted processing for %s with %s %s %s.' % (account, errno, errstr, trcbck))


if __name__ == '__main__':
    main(sys.argv[1:])

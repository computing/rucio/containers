#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright (C) 2020 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
"""rse_distances
Sets distances and rankings for RSEs to unity
"""
import argparse
from rucio.client.rseclient import RSEClient
import rucio.common.exception


def get_rse_list(client):
    """
    Return a python list of RSEs
    """
    return [r['rse'] for r in client.list_rses()]


def main():
    """Core functionality"""

    rseclient = RSEClient()
    all_rses = get_rse_list(rseclient)

    for src in all_rses:
        for dst in all_rses:
            if src == dst:
                continue
            print("RSEs: {0}, {1}".format(src, dst))
            try:
                rseclient.add_distance(source=src, destination=dst,
                                       parameters={'distance': 1,
                                                   'ranking': 1})
            except rucio.common.exception.RucioException:
                rseclient.update_distance(source=src, destination=dst,
                                          parameters={'distance': 1,
                                                      'ranking': 1})

            try:
                rseclient.add_distance(source=dst, destination=src,
                                       parameters={'distance': 1,
                                                   'ranking': 1})
            except rucio.common.exception.RucioException:
                rseclient.update_distance(source=dst, destination=src,
                                          parameters={'distance': 1,
                                                      'ranking': 1})

            print("Distance {src}->{dst}: {distance}".format(
                src=src, dst=dst,
                distance=rseclient.get_distance(src, dst)[0]['distance']
            ))
            print("Distance {dst}->{src}: {distance}".format(
                src=src, dst=dst,
                distance=rseclient.get_distance(dst, src)[0]['distance']
            ))

if __name__ == "__main__":
    main()

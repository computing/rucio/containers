#!/bin/bash -e

if [ ! -f /opt/rucio/etc/rucio.cfg ]; then
    echo "File rucio.cfg not found. It will generate one."
    mkdir -p /opt/rucio/etc/
    j2 /opt/user/rucio.cfg.j2 > /opt/rucio/etc/rucio.cfg
fi

echo "--- Synchronizing initial IGWN configuration ---"

echo "Stage 1: Create initial accounts and RSEs"

echo "Creating accounts (sync_accounts.py)"
/config/tools/sync_accounts.py

echo "Creating RSEs (sync_rses.py)"
/config/tools/sync_rses.py

echo "Stage 2: Additional RSE configuration and metadata"
/config/tools/sync_distances.py
/config/tools/enable_fts.sh
/config/tools/sync_meta.py

echo "Stage 3: Create TEST scope"
rucio-admin scope add --scope test --account root

echo "Stage 4: delete bootstrap account"
rucio-admin identity delete --account root --type USERPASS --id bootstrap


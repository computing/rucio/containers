#!/bin/bash -e
for rse in $(rucio-admin rse list); do
  rucio-admin rse set-attribute --rse ${rse} --key fts --value https://fts.mwt2.org:8446
done

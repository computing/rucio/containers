#!/bin/bash -e
COPYCMD="/usr/bin/gfal-copy -f"
STATCMD="/usr/bin/gfal-stat"

log_event(){
  event=$1
  log=$(printf '[%s %s %s]\n' "$(basename $0)" "$(date --rfc-3339=seconds)")
  echo "$log $event"
}

print_usage(){
  echo "Usage: SRC=/source DEST=/destination $(basename $0)"
  echo "SRC and DEST are environment variables:"
  echo -e "SRC \t source file (e.g. gsiftp://host:port/path)"
  echo -e "DST \t destination file(s). (e.g., /new_path)"
}


if [ -z "${SRC}" ]; then
  echo "Please set SRC"
  print_usage
  exit 1
fi
if [ -z "${DEST}" ]; then
  echo "Please set DEST"
  print_usage
  exit 1
fi
if [ -z "${MAX_RETRIES}" ]; then
  MAX_RETRIES=5
  echo "Using MAX_RETRIES=${MAX_RETRIES}"
fi

if [[ -z "${DAEMON}" || "${DAEMON}" != "True" && "${DAEMON}" != "true" ]]; then
  log_event "Not running in daemon mode"
  DAEMON="False"
else
  log_event "Running in daemon mode"
  DAEMON="True"
  if [ -z "${SLEEP_INTERVAL}" ]; then
    SLEEP_INTERVAL=5
  fi
  log_event "Setting sleep interval to ${SLEEP_INTERVAL}"
  if [ -z "${RETRY_SLEEP_INTERVAL}" ]; then
    RETRY_SLEEP_INTERVAL=5
  fi
  log_event "Setting INITIAL retry sleep interval to ${RETRY_SLEEP_INTERVAL}"
fi

RETRY_SLEEP_INTERVAL_0=${RETRY_SLEEP_INTERVAL}

get_cache_epoch(){
  retry=1
  nretry=1
  until stat_out=$(gfal-stat $SRC); do
    RETRY_SLEEP_INTERVAL=$((${RETRY_SLEEP_INTERVAL}*2))
    log_event "Trying again in ${RETRY_SLEEP_INTERVAL} seconds ($nretry/$MAX_RETRIES)"

    if [ ${nretry} -ge ${MAX_RETRIES} ]; then
      log_event "Maximum retries reached: connectivity issue"
      exit 1
    fi
    nretry=$((nretry+1))

    sleep ${RETRY_SLEEP_INTERVAL}
  done
  RETRY_SLEEP_INTERVAL=${RETRY_SLEEP_INTERVAL_0}
  # Hack a return value so we preserve diagnostics
  export epoch=$(date -d "$(gfal-stat $SRC | grep Modify | sed 's/Modify://')" +%s)
}

while /bin/true; do

  log_event "Getting mtime of ${SRC}"

  get_cache_epoch

  log_event "Seconds since last frame cache modification: $(($(date +%s) - ${epoch}))"

  log_event "Fetching: ${SRC}"
  retry=1
  nretry=1
  until ${COPYCMD} ${SRC} ${DEST}.tmp; do
    RETRY_SLEEP_INTERVAL=$((${RETRY_SLEEP_INTERVAL}*2))
    log_event "Trying again in ${RETRY_SLEEP_INTERVAL} seconds ($nretry/$MAX_RETRIES)"

    if [ ${nretry} -ge ${MAX_RETRIES} ]; then
      echo "Maximum retries reached: connectivity issue"
      exit 1
    fi
    nretry=$((nretry+1))

    sleep ${RETRY_SLEEP_INTERVAL}
  done
  RETRY_SLEEP_INTERVAL=${RETRY_SLEEP_INTERVAL_0}

  if [ -s ${DEST}.tmp ]; then
    log_event "Available at DEST: ${DEST}"
    mv ${DEST}.tmp ${DEST}
  else
    log_event "${DEST}.tmp not found or is empty"
  fi

  if [ ${DAEMON} != "True" ]; then
    exit 0
  fi

  log_event "Going to sleep for ${SLEEP_INTERVAL} seconds"
  sleep ${SLEEP_INTERVAL}

done

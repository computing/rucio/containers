FROM centos:7

RUN rpm -Uvh https://repo.opensciencegrid.org/osg/3.6/osg-3.6-el7-release-latest.rpm  && \
      yum install -y osg-ca-certs epel-release

RUN yum install -y gfal2-util gfal2-all && \
      yum clean all && \
      rm -rf /var/cache/yum

COPY gfal-copy.sh /gfal-copy.sh
ENTRYPOINT ["/gfal-copy.sh"]
CMD ["/bin/bash"]
